import 'package:flutter_boilerplate_template/app/app.dart';
import 'package:flutter_boilerplate_template/shared/index.dart';
import 'package:flutter_boilerplate_template/startup.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: '.env.staging');

  await SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
    ],
  ).then((_) {
    run(() => const App());
  });
}
