import 'package:flutter_boilerplate_template/shared/constants/index.dart';
import 'package:flutter_boilerplate_template/shared/infrastructure/index.dart';
import 'package:flutter_boilerplate_template/shared/staticdata/static_data.dart';

class App extends StatelessWidget {
  const App({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: AppConstants.fontFamily,
        //canvasColor: Colors.transparent,
      ),

      onGenerateRoute: AppRoute.onGenerateRoute,
      initialRoute: '/repo-list',
      //initialRoute: '/test',
    );
  }
}
