class AppConstants {
  static const String fontFamily = 'SF Pro Display';
  static const String logoFontFamily = 'Dosis';
  static const int otpTimeInSeconds = 120;
  static const int connectionTimeOut = 35000; //35s
  static const int responseTimeOut = 35000; //35s
  static const int debounceTime = 500;
  static const String languageCode = 'en';
  static const String helpContactNumber = '+88 01700000000';

  static String userImagePlaceHolder =
      'https://images.vexels.com/media/users/3/137632/isolated/preview/c7915d9b389bd7b3764fb11445b53bba-simple-infinity-logo-infinite.png';

  static const String emptyGuId = '00000000-0000-0000-0000-000000000000';
}
