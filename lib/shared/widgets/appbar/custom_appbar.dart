import 'package:flutter_boilerplate_template/shared/index.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    super.key,
    this.title = '',
    this.subTitle,
    this.icon = 'icons/arrow_back.svg',
    this.hasBackIcon = true,
    this.elevation = 0,
    this.children = const <Widget>[],
    this.titleWidget,
    this.onPressed,
    // ignore: avoid_field_initializers_in_const_classes
  }) : preferredSize = const Size.fromHeight(appBarHeight);

  final String title;
  final String? subTitle;
  final bool hasBackIcon;
  final String icon;
  final double elevation;
  final Widget? titleWidget;
  final List<Widget>? children;
  final VoidCallback? onPressed;

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: elevation,
      shadowColor: const Color(0x00000000).withOpacity(0.20),
      centerTitle: false,
      leadingWidth: 60,
      titleSpacing: hasBackIcon ? 0 : screenLeftPadding,
      toolbarHeight: appBarHeight,
      automaticallyImplyLeading: false,
      shape: const Border(
        bottom: BorderSide(color: grey200),
      ),
      leading: hasBackIcon
          ? Transform(
              transform: Matrix4.translationValues(-2, 0, 0),
              child: IconButton(
                padding: EdgeInsets.zero,
                constraints: const BoxConstraints(),
                splashRadius: iconSplashRadius,
                icon: const Icon(
                  Icons.navigate_before,
                  size: 30,
                ),
                onPressed: onPressed,
              ),
            )
          : null,
      title: titleWidget ??
          CustomText(
            text: title,
            // fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
      actions: children != null ? [...children!] : null,
    );
  }
}
