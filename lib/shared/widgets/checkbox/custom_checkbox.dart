import 'package:flutter_boilerplate_template/shared/index.dart';

class CustomCheckBox extends StatelessWidget {
  const CustomCheckBox({
    super.key,
    this.isRectangle = true,
    this.onChanged,
    this.value = false,
  });
  final bool isRectangle;
  // ignore: avoid_positional_boolean_parameters
  final void Function(bool?)? onChanged;
  final bool? value;

  @override
  Widget build(BuildContext context) {
    return Transform(
      transform: Matrix4.translationValues(-9, 0, 0),
      child: Transform.scale(
        scale: 1.2,
        child: Checkbox(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: const BorderSide(
            width: 2,
          ),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(2),
            ),
          ),
          activeColor: brand,
          value: value,
          onChanged: onChanged,
        ),
      ),
    );
  }
}
