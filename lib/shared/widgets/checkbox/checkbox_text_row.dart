import 'package:flutter_boilerplate_template/shared/index.dart';

class CheckBoxTextRow extends StatelessWidget {
  const CheckBoxTextRow({
    required this.label,
    this.labelTextSize = 16,
    this.labelFontWeight = FontWeight.w400,
    this.value = false,
    this.widget,
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.onChanged,
    super.key,
  });

  final String label;
  final double labelTextSize;
  final FontWeight labelFontWeight;
  final bool value;
  final Widget? widget;
  final CrossAxisAlignment crossAxisAlignment;
  // ignore: avoid_positional_boolean_parameters
  final void Function(bool?)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: crossAxisAlignment,
      children: [
        CustomCheckBox(
          value: value,
          onChanged: onChanged,
        ),
        const SizedBox(
          width: 6,
        ),
        Expanded(
          child: GestureDetector(
            onTap: () {
              onChanged!(!value);
            },
            child: widget ??
                CustomText(
                  text: label,
                  fontSize: labelTextSize,
                  fontWeight: labelFontWeight,
                ),
          ),
        ),
      ],
    );
  }
}
