import 'package:flutter_boilerplate_template/shared/index.dart';

class Loader extends StatelessWidget {
  const Loader({
    super.key,
    this.color = brand,
    this.strokeWidth = 2,
  });
  final Color color;
  final double strokeWidth;

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      strokeWidth: strokeWidth,
      valueColor: AlwaysStoppedAnimation<Color>(color),
    );
  }
}
