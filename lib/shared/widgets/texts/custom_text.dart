import 'package:flutter_boilerplate_template/shared/index.dart';

class CustomText extends StatelessWidget {
  const CustomText({
    required this.text,
    this.color = primaryBlack,
    this.fontSize = 16,
    this.fontWeight = FontWeight.w400,
    this.height,
    this.textAlign = TextAlign.left,
    this.fontFamily = AppConstants.fontFamily,
    this.maxLine,
    this.overflow,
    this.decoration,
    this.fontStyle,
    this.softWrap,
    super.key,
  });

  final String text;
  final Color color;
  final double fontSize;
  final FontWeight fontWeight;
  final double? height;
  final TextAlign textAlign;
  final String fontFamily;
  final int? maxLine;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  final FontStyle? fontStyle;
  final bool? softWrap;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      maxLines: maxLine,
      overflow: overflow,
      softWrap: softWrap,
      style: TextStyle(
        fontWeight: fontWeight,
        fontSize: fontSize,
        color: color,
        height: height,
        fontFamily: fontFamily,
        decoration: decoration,
        fontStyle: fontStyle,
      ),
    );
  }
}
