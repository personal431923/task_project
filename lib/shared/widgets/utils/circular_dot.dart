import 'package:flutter_boilerplate_template/shared/index.dart';

class CircularDot extends StatelessWidget {
  const CircularDot({
    super.key,
    this.color = brand,
    this.radius = 8,
  });

  final Color color;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: radius,
      height: radius,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}
