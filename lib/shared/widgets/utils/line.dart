import 'package:flutter_boilerplate_template/shared/index.dart';

class Line extends StatelessWidget {
  const Line({
    super.key,
    this.color = grey200,
    this.thickness = 1,
  });

  final Color color;
  final double thickness;

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 0,
      color: color,
      thickness: thickness,
    );
  }
}
