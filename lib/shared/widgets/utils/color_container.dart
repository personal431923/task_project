import 'package:flutter_boilerplate_template/shared/index.dart';

class ColorContainer extends StatelessWidget {
  const ColorContainer({
    required this.colorCode,
    required this.colorName,
    this.isSelected = false,
    this.onPressed,
    super.key,
  });
  final String colorCode;
  final String colorName;
  final bool isSelected;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkResponse(
          onTap: onPressed,
          radius: 32,
          child: Container(
            height: 32,
            width: 32,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(2),
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              // color: colorCode.toColor(),
            ),
            child: isSelected
                ? const Icon(
                    Icons.check,
                    color: Colors.white,
                  )
                : const SizedBox(),
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        CustomText(
          text: colorName,
        ),
      ],
    );
  }
}
