import 'package:flutter_boilerplate_template/shared/index.dart';

class ServerErrorContainer extends StatelessWidget {
  const ServerErrorContainer({
    super.key,
    this.isPullRefresh = false,
    this.onRefresh,
    this.topPadding,
  });

  final bool isPullRefresh;
  final Future<void> Function()? onRefresh;
  final double? topPadding; //imageHeight + Gap

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      // ignore: avoid_returning_null_for_void
      onRefresh: isPullRefresh ? onRefresh! : () async => false,
      notificationPredicate: isPullRefresh ? (_) => true : (_) => false,
      color: brand,
      child: SingleChildScrollView(
        physics: isPullRefresh
            ? const AlwaysScrollableScrollPhysics()
            : const NeverScrollableScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.only(
            top: topPadding ??
                ((SizeUtils.screenHeight -
                            (SizeUtils.topPadding + appBarHeight)) /
                        2) -
                    (160 + 20 + 30),
            left: screenLeftPadding,
            right: screenRightPadding,
          ),
          child: const Column(
            children: [
              CustomText(
                text: 'Something went wrong!',
                fontSize: 20,
                color: brand,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 16,
              ),
              CustomText(
                text: 'Try again later.',
                fontSize: 14,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
