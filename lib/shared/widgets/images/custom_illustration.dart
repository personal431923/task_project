import 'package:flutter/material.dart';

class CustomIllustration extends StatelessWidget {
  const CustomIllustration({
    required this.height,
    required this.src,
    this.width,
    this.fit,
    this.child,
    super.key,
  });

  final double height;
  final double? width;
  final String src;
  final BoxFit? fit;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/$src',
          ),
          fit: fit,
        ),
      ),
      child: child,
    );
  }
}
