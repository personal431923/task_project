import 'package:flutter_boilerplate_template/shared/index.dart';

class NavigateCard extends StatelessWidget {
  const NavigateCard({
    required this.name,
    required this.starsCount,
    required this.owner,
    this.onTap,
    super.key,
  });
  final String name;
  final String starsCount;
  final String owner;

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Ink(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: const BorderRadius.all(Radius.circular(4)),
          border: Border.all(color: grey200),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  text: 'name: $name',
                  fontWeight: FontWeight.w600,
                ),
                CustomText(
                  text: 'number of stars: $starsCount',
                  fontWeight: FontWeight.w600,
                ),
                CustomText(
                  text: 'owner: $owner',
                  fontWeight: FontWeight.w600,
                ),
              ],
            ),
            IconButton(onPressed: onTap, icon: const Icon(Icons.navigate_next)),
          ],
        ),
      ),
    );
  }
}
