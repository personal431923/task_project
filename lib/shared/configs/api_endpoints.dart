// ignore_for_file: non_constant_identifier_names

import 'package:flutter_dotenv/flutter_dotenv.dart';

class APIEndpoints {
  static final String APP_BASE_URL = dotenv.get('APP_BASE_URL');
}
