export 'package:flutter/material.dart';
export 'package:flutter_boilerplate_template/modules/repository/ui/screen/repository_detail_screen.dart';
export 'package:flutter_boilerplate_template/modules/repository/ui/screen/repository_list_screen.dart';
export 'package:page_transition/page_transition.dart';

export 'app_route.dart';
