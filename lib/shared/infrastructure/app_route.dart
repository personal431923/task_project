import 'package:flutter_boilerplate_template/shared/infrastructure/index.dart';

class AppRoute {
  static Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/repo-list':
        return PageTransition<dynamic>(
          child: const RepositoryListScreen(),
          type: PageTransitionType.rightToLeft,
          settings: settings,
        );
      case '/repo-detail':
        final params = settings.arguments! as Map<String, dynamic>;

        return PageTransition<dynamic>(
          child: RepositoryDetailScreen(
            repo: params['repo'],
          ),
          type: PageTransitionType.rightToLeft,
          settings: settings,
        );

      default:
        return PageTransition<dynamic>(
          child: Container(),
          type: PageTransitionType.rightToLeft,
          settings: settings,
        );
    }
  }
}
