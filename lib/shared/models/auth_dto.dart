class AuthDTO {
  AuthDTO({
    required this.userID,
    required this.accessToken,
  });

  AuthDTO.fromJson(Map<String, dynamic> json) {
    userID = json['id'];
    accessToken = json['authtoken'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};

    data['id'] = userID;
    data['authtoken'] = accessToken;

    return data;
  }

  late String userID;
  late String accessToken;
}
