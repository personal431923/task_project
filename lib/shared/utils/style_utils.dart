const double screenLeftPadding = 20;
const double screenTopPadding = 20;
const double screenRightPadding = 20;
const double screenBottomPadding = 20;
const double screenAllSidePadding = 20;

const double inputFieldBorderRadius = 5;
const double btnBorderRadius = 5;
const double bottomSheetBorderRadius = 20;
const double borderWidth = 1;

const double appBarHeight = 64;
const double iconSplashRadius = 30;
const double refreshIndicatorPlacementHeight = 4;
