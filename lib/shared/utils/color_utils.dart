import 'package:flutter/material.dart';

const Color brand = Color(0xFF37CD7C);
const Color secondaryBrand = Color(0xFF37CEBA);
const Color primaryBlack = Color(0xFF20202E);
const Color primaryWhite = Color(0xFFFFFFFF);
const Color backgroundColor = primaryWhite;
const Color grey200 = Color(0xFFEDEEF1);

//color set
const Color emerald400 = Color(0xFF194E33);
const Color emerald300 = Color(0xFF1D7847);
const Color emerald200 = Color(0xFF8EE7B6);
const Color emerald100 = Color(0xFFDEFAEB);
const Color emerald50 = Color(0xFFF1FCF5);
const Color turquoise400 = Color(0xFF154C48);
const Color turquoise300 = Color(0xFF137269);
const Color turquoise200 = Color(0xFF9DF2E1);
const Color turquoise100 = Color(0xFFCEF9F0);
const Color turquoise50 = Color(0xFFF1FCFA);
const Color grey900 = Color(0xFF40444C);
const Color grey800 = Color(0xFF4A4E5A);
const Color grey700 = Color(0xFF5B616E);
const Color grey600 = Color(0xFF6B7280);
const Color grey500 = Color(0xFF8E95A2);
const Color grey400 = Color(0xFFB6BAC3);
const Color grey300 = Color(0xFFD8DBDF);
