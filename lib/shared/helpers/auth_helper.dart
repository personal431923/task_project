import 'package:flutter_boilerplate_template/shared/index.dart';

class AuthHelper {
  static bool isAuthenticated() => tokenInfo() != null;

  static String accessToken() =>
      tokenInfo() != null ? tokenInfo()!.accessToken : '';

  static void clearToken() =>
      DbHelper.saveData(Tables.appUtils, AppKeys.tokenInfo, '');

  static AuthDTO? tokenInfo() {
    AuthDTO? tokenInfo;
    final response = DbHelper.getData(Tables.appUtils, AppKeys.tokenInfo);

    // ignore: avoid_dynamic_calls
    if (response.isNotEmpty) {
      final data = jsonDecode(response);
      if (data != null) {
        tokenInfo = AuthDTO.fromJson(data);
      }
    }

    return tokenInfo;
  }

  static void logOut() {
    clearToken();
  }
}
