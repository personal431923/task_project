// ignore_for_file: avoid_dynamic_calls

import 'dart:io' show Platform;

import 'package:flutter_boilerplate_template/shared/index.dart';

class AppHelper {
  static int appPlatform() => Platform.isAndroid ? 2 : 3; //2 Android and 3 Ios

  static void closeApp() =>
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
}
