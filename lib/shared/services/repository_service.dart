import 'package:flutter_boilerplate_template/shared/configs/index.dart';
import 'package:flutter_boilerplate_template/shared/index.dart';

class RepositoryService {
  Future<HttpResponse> getFlutterRepoSitories({
    int page = 1,
    int perPage = 10,
  }) async {
    final url = '${APIEndpoints.APP_BASE_URL}/search/repositories';
    // final url =
    //     '${APIEndpoints.APP_BASE_URL}/search/repositories?q=flutter&page=$page&per_page=$perPage&sort=stars&order=desc';

    final params = {
      'q': 'flutter',
      'page': page.toString(),
      'per_page': perPage.toString(),
      'sort': 'stars',
      'order': 'desc',
    };

    final response = await HttpService.get(url, queryParameters: params);

    return response;
  }
}
