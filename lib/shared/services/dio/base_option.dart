import 'package:dio/dio.dart';
import 'package:flutter_boilerplate_template/shared/constants/index.dart';

BaseOptions baseOptions = BaseOptions(
  //baseUrl: APIEndpoints.baseUrl,
  connectTimeout: const Duration(seconds: AppConstants.connectionTimeOut),
  receiveTimeout: const Duration(seconds: AppConstants.responseTimeOut),
);
