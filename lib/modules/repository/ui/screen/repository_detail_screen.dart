import 'package:flutter_boilerplate_template/modules/repository/repository.dart';

class RepositoryDetailScreen extends StatelessWidget {
  const RepositoryDetailScreen({
    required this.repo,
    super.key,
  });

  final RepositoryItem repo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: repo.name ?? '',
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      body: Padding(
        padding: const EdgeInsets.all(screenAllSidePadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              text: 'Owner: ${repo.owner!.login ?? ''}',
            ),
            CustomText(
              text: 'Last Update: ${repo.updatedAt ?? ''}',
            ),
            CustomText(
              text: 'Description: ${repo.description ?? ''}',
            ),
          ],
        ),
      ),
    );
  }
}
