// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter_boilerplate_template/modules/repository/repository.dart';

class RepositoryListScreen extends StatelessWidget {
  const RepositoryListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          RepositoryListBloc(repositoryService: RepositoryService())
            ..add(const GetRepositoryList()),
      child: BuildRepositorylist(),
    );
  }
}

class BuildRepositorylist extends StatelessWidget {
  BuildRepositorylist({super.key});

  late RepositoryListBloc _repositoryListBloc;
  final _scrollController = ScrollController();

  void _initScrollListener(BuildContext context) {
    _scrollController.addListener(() {
      if (_scrollController.position.atEdge) {
        if (_scrollController.position.pixels != 0) {
          _repositoryListBloc.add(
            const GetMoreRepos(),
          );
        }
      }
    });
  }

  Future<void> _pullRefresh(BuildContext context) async {
    _repositoryListBloc.add(
      const GetRepositoryList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    _initScrollListener(context);
    _repositoryListBloc = BlocProvider.of<RepositoryListBloc>(context);
    SizeUtils().init(context);

    return Scaffold(
      appBar: const CustomAppBar(
        title: 'Flutter Repo List',
        hasBackIcon: false,
      ),
      body: BlocBuilder<RepositoryListBloc, RepositoryListState>(
        builder: (context, state) {
          if (state is RepositoryListLoading ||
              state is RepositoryListInitial) {
            return const Center(
              child: Loader(),
            );
          } else if (state is RepositoryListLoaded) {
            return RefreshIndicator(
              onRefresh: () => _pullRefresh(context),
              color: brand,
              child: Column(
                children: [
                  Expanded(
                    child: ListView.separated(
                      controller: _scrollController,
                      shrinkWrap: true,
                      physics: const AlwaysScrollableScrollPhysics(),
                      separatorBuilder: (context, index) => const Padding(
                        padding: EdgeInsets.only(
                          left: screenLeftPadding,
                          right: screenRightPadding,
                        ),
                        child: Line(),
                      ),
                      itemCount: state.repositoryList.length,
                      itemBuilder: (context, index) {
                        final repo = state.repositoryList[index];

                        return NavigateCard(
                          name: repo.name ?? '',
                          starsCount: repo.stargazersCount.toString(),
                          owner: repo.owner!.login ?? '',
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              '/repo-detail',
                              arguments: {
                                'repo': repo,
                              },
                            );
                          },
                        );
                      },
                    ),
                  ),
                  BlocBuilder<RepositoryListBloc, RepositoryListState>(
                    builder: (__, state) {
                      return state is RepositoryListLoaded && state.isLoading
                          ? const Padding(
                              padding: EdgeInsets.only(top: 4),
                              child: Center(
                                child: CupertinoActivityIndicator(
                                  radius: 12,
                                ),
                              ),
                            )
                          : const SizedBox();
                    },
                  ),
                ],
              ),
            );
          } else if (state is RepositoryListEmpty) {
            return const Center(child: CustomText(text: 'No repo found'));
          } else {
            return ServerErrorContainer(
              onRefresh: () => _pullRefresh(context),
            );
          }
        },
      ),
    );
  }
}
