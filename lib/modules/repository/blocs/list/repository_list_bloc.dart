// ignore_for_file: avoid_dynamic_calls, unnecessary_lambdas

import 'package:flutter_boilerplate_template/modules/repository/repository.dart';

class RepositoryListBloc
    extends Bloc<RepositoryListEvent, RepositoryListState> {
  RepositoryListBloc({required this.repositoryService})
      : super(const RepositoryListInitial()) {
    on<GetRepositoryList>(_getRepositoryList);
    on<GetMoreRepos>(_getMoreRepos);
  }

  final RepositoryService repositoryService;
  List<RepositoryItem> repositoryList = [];
  int currentPage = 1;
  int totalCount = 0;

  Future<void> _getRepositoryList(
    GetRepositoryList event,
    Emitter<RepositoryListState> emit,
  ) async {
    repositoryList = [];
    currentPage = 1;
    emit(RepositoryListLoading());
    final response = await repositoryService.getFlutterRepoSitories();

    if (response.error == null && response.payload != null) {
      final payload = jsonDecode(response.payload);

      final List list = payload['items'];
      totalCount = payload['total_count'];

      repositoryList =
          list.map((item) => RepositoryItem.fromJson(item)).toList();

      if (repositoryList.isNotEmpty) {
        emit(
          RepositoryListLoaded(
            repositoryList: repositoryList,
          ),
        );
      } else {
        emit(const RepositoryListEmpty());
      }
    } else {
      emit(
        RepositoryListError(
          errors: response.error!.messages ?? {},
        ),
      );
    }
  }

  Future<void> _getMoreRepos(
    GetMoreRepos event,
    Emitter<RepositoryListState> emit,
  ) async {
    if (repositoryList.length == totalCount) {
      return;
    }

    emit(
      RepositoryListLoaded(
        repositoryList: repositoryList,
        isLoading: true,
      ),
    );

    final response = await repositoryService.getFlutterRepoSitories(
      page: currentPage += 1,
    );
    if (response.error == null && response.payload != null) {
      final payload = jsonDecode(response.payload);
      final List list = payload['items'];
      totalCount = payload['total_count'];
      repositoryList +=
          list.map((item) => RepositoryItem.fromJson(item)).toList();

      emit(
        RepositoryListLoaded(
          repositoryList: repositoryList,
        ),
      );
    } else {
      emit(
        RepositoryListError(
          errors: response.error!.messages ?? {},
        ),
      );
    }
  }
}
