import 'package:equatable/equatable.dart';
import 'package:flutter_boilerplate_template/shared/models/repository_item.dart';

abstract class RepositoryListState extends Equatable {
  const RepositoryListState();
}

class RepositoryListInitial extends RepositoryListState {
  const RepositoryListInitial();

  @override
  List<Object?> get props => [];
}

class RepositoryListLoading extends RepositoryListState {
  @override
  List<Object?> get props => [];
}

class RepositoryListLoaded extends RepositoryListState {
  const RepositoryListLoaded({
    required this.repositoryList,
    this.isLoading = false,
  });

  final List<RepositoryItem> repositoryList;
  final bool isLoading;

  @override
  List<Object?> get props => [
        repositoryList,
        isLoading,
      ];
}

class RepositoryListEmpty extends RepositoryListState {
  const RepositoryListEmpty();

  @override
  List<Object?> get props => [];
}

class RepositoryListError extends RepositoryListState {
  const RepositoryListError({required this.errors});

  final Map<String, String> errors;

  @override
  List<Object?> get props => [errors];
}
