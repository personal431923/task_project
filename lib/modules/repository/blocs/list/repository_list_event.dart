import 'package:equatable/equatable.dart';

abstract class RepositoryListEvent extends Equatable {
  const RepositoryListEvent();
}

class GetRepositoryList extends RepositoryListEvent {
  const GetRepositoryList();

  @override
  List<Object?> get props => [];
}

class GetMoreRepos extends RepositoryListEvent {
  const GetMoreRepos();

  @override
  List<Object?> get props => [];
}
