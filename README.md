# Project task

A template for flutter app

---

## Getting Started 🚀

This project contains 3 environments:

- development
- staging
- production

To run the desired environment either use the launch configuration in VSCode/Android Studio or use the following commands:

```sh
# Development
$ flutter run --flavor development --target lib/main_development.dart

# Staging
$ flutter run --flavor staging --target lib/main_staging.dart

# Production
$ flutter run --flavor production --target lib/main_production.dart
```
